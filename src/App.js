import React from "react";
import XLSX from "xlsx";
import ReactJsAlert from "reactjs-alert";
import {abecedary as abc} from "./exports.js"
import './App.css';

export default class SheetJSApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [] ,
      cols: [] ,
        coords: '',
        type: "",
      status: false,
      title: "",
    };
    this.handleFile = this.handleFile.bind(this);
    this.exportFile = this.exportFile.bind(this);
    this.serviceLogin = this.serviceLogin.bind(this);
    this.saveData = this.saveData.bind(this);
    this.newRow = this.newRow.bind(this);
    this.newCol = this.newCol.bind(this);
    this.setValue = this.setValue.bind(this);
    this.cellDelete = this.cellDelete.bind(this);
    this.cellAdd = this.cellAdd.bind(this);
    this.rowAdd = this.rowAdd.bind(this);
    this.rowDelete = this.rowDelete.bind(this);
    this.setCoords = this.setCoords.bind(this);
  }
  handleFile(file /*:File*/) {
    /* Boilerplate to set up FileReader */
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;
    reader.onload = e => {
      /* Parse data */
      const bstr = e.target.result;
      const wb = XLSX.read(bstr, { type: rABS ? "binary" : "array" });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      console.log(rABS, wb);
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws, { header: 1 });
      /* Update state */
        console.log('datos de archivo', data);
      this.setState({ data: data, cols: make_cols(ws["!ref"]) });
    };
    if (rABS) reader.readAsBinaryString(file);
    else reader.readAsArrayBuffer(file);
  }
    async serviceLogin() {
        const params = {
            "email": "admin@admins",
            "password":"123456",
            "remember_me": true
        };

        const login = await fetch('http://47.88.32.26/Neo-test/public/api/auth/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(params)
        });

        return login;
    }

    async exportFile() {
        await this.serviceLogin().then(response => response.json())
        .then(repos => {
           if(repos.access_token) {
                fetch('http://47.88.32.26/Neo-test/public/api/auth/user', {
                    method: 'GET',
                    headers: { 'Content-Type': 'application/json', 'Authorization': repos.token_type+' '+repos.access_token },
                }).then(response => response.json())
                .then(resp => {
                    var array = resp.map(function(o) {
                        return Object.keys(o).reduce(function(array, key) {
                            return array.concat([o[key]]);
                        }, []);
                    });
                    const ws = XLSX.utils.aoa_to_sheet(array);

                    const wb = XLSX.utils.book_new();
                    XLSX.utils.book_append_sheet(wb, ws, "SheetJS");

                    XLSX.writeFile(wb, "sheetjs.csv");
                })
                .catch(err => console.log(err))

           } else {
               console.log('no conection')
           }
        })
        .catch(err => console.log(err))
    }

    async saveData() {
        let usersArray = this.state.data;

        await this.serviceLogin().then(response => response.json())
        .then(repos => {
           if(repos.access_token) {
            console.log('respuesta',repos.access_token);

                fetch('http://47.88.32.26/Neo-test/public/api/auth/usersimport', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json', 'Authorization': repos.token_type+' '+repos.access_token },
                    body: JSON.stringify({"arreglo": usersArray})
                }).then(response => response.json())
                .then(resp => {
                    if(resp.uncomplete) {
                        var uncompleteList = JSON.parse(resp.uncomplete);
                        var rowList = '';

                        if(uncompleteList.length > 0) {
                            for(var i = 0 ; i < uncompleteList.length ; i++) {
                                rowList += uncompleteList[i]+', '
                            }

                            this.setState({type: "error"});
                            this.setState({title: 'La(s) fila(s) '+rowList+' no fue(ron) procesada(s), Username y Nombre son campos requeridos.' });
                        } else {
                            this.setState({type: "success"});
                            this.setState({title: 'Los datos se han guardado exitosamente.' });
                        }

                        this.setState({status: true });

                    } else {
                       console.log('no conection')
                    }
                })
                .catch(err => console.log(err))
                console.log('datos para enviar', usersArray)

           } else {
               console.log('no conection')
           }
        })
        .catch(err => console.log(err))
        console.log('datos para enviar', usersArray)

    }

    newRow() {
        var swap = this.state.data
        swap.push([]);
        this.setState({ data: swap});
    }

    newCol() {
        var swap = this.state.cols
        swap.push({name:abc[this.state.cols.length], key:this.state.cols.length});
        this.setState({ cols: swap});
        console.log(this.state.cols.length)
    }

    setValue(row, col, value) {

        this.state.data[row][col] = value
        this.setState({ data: this.state.data});

                console.log('fila'+row+'columna'+col+'valor'+ value)
    }

    cellDelete() {
        let row = parseInt(this.state.coords.split(',')[0]);
        let col = parseInt(this.state.coords.split(',')[1]);

        for(var i = row; i < this.state.data.length; i++) {
            if(i === this.state.data.length-1) {
                this.state.data[i][col] = '';
            } else {
                this.state.data[i][col] = this.state.data[i+1][col];
            }
        }
        this.setState({ data: this.state.data});
    }

    cellAdd() {
        let row = parseInt(this.state.coords.split(',')[0]);
        let col = parseInt(this.state.coords.split(',')[1]);
        var swap = this.state.data
        swap.push([]);
        this.setState({ data: swap});
        console.log('fila'+row+'col'+this.state.data.length)
        setTimeout(()=>{
            for(var i =  this.state.data.length-1; row <= i ; i--) {
                if(i === row) {
                    this.state.data[i][col] = '';
                } else {
                    this.state.data[i][col] = (this.state.data[i-1][col]) ? this.state.data[i-1][col] : "";
                }
            }
            this.setState({ data: this.state.data});
        },200);

    }

    rowDelete() {
        let row = parseInt(this.state.coords.split(',')[0]);
        var swap = this.state.data
        swap.splice(row,1)

        this.setState({ data: swap});
    }

    rowAdd() {
        let row = parseInt(this.state.coords.split(',')[0]);

        var swap = this.state.data
        swap.push([]);
        this.setState({ data: swap});

        setTimeout(()=>{
        for(var i =  this.state.data.length-1; row <= i ; i--) {
            if(i === row) {
                for(var j = 0; j < this.state.data[i].length; j++){
                    this.state.data[i][j] = '';
                }
            } else {
                this.state.data[i] = this.state.data[i-1];
            }
        }
        this.setState({ data: this.state.data});
        },200)

    }

    setCoords(row, col) {
        this.setState({coords: row+','+col})
    }

  render() {
    return (
      <DragDropFile handleFile={this.handleFile}>
        <div className="row">
          <div className="col-xs-12">
            <DataInput handleFile={this.handleFile} />
          </div>
        </div>
        <div className="menu">
          <div className="col-xs-12">
            <button
              disabled={!this.state.data.length}
                className="menu-buttons"
              onClick={this.saveData}
            >
              Salvar Datos
            </button>
            <button
              disabled={!this.state.data.length}
                className="menu-buttons"
              onClick={this.exportFile}
            >
              Exportar CSV
            </button>
            <button
              disabled={!this.state.data.length}
                className="menu-buttons"
              onClick={this.newRow}
            >
              nueva fila
            </button>
            <button
              disabled={!this.state.data.length}
                className="menu-buttons"
              onClick={this.newCol}
            >
              nueva columna
            </button>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <OutTable data={this.state.data} cols={this.state.cols} setNewValue={ this.setValue }/>
          </div>
        </div>
        <ReactJsAlert
          status={this.state.status} // true or false
          type={this.state.type} // success, warning, error, info
          title={this.state.title}
          Close={() => this.setState({ status: false })}
            button="cerrar"
        />
        <ContextMenu
            setCoords={this.setCoords}
            cellDelete={this.cellDelete}
            cellAdd={this.cellAdd}
            rowDelete={this.rowDelete}
            rowAdd={this.rowAdd}
        />
      </DragDropFile>
    );
  }
}

/* -------------------------------------------------------------------------- */

/*
  Simple HTML5 file drag-and-drop wrapper
  usage: <DragDropFile handleFile={handleFile}>...</DragDropFile>
    handleFile(file:File):void;
*/
class DragDropFile extends React.Component {
  constructor(props) {
    super(props);
    this.onDrop = this.onDrop.bind(this);
  }
  suppress(evt) {
    evt.stopPropagation();
    evt.preventDefault();
  }
  onDrop(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    const files = evt.dataTransfer.files;
    if (files && files[0]) this.props.handleFile(files[0]);
  }
  render() {
    return (
      <div
        onDrop={this.onDrop}
        onDragEnter={this.suppress}
        onDragOver={this.suppress}
      >
        {this.props.children}
      </div>
    );
  }
}

/*
  Simple HTML5 file input wrapper
  usage: <DataInput handleFile={callback} />
    handleFile(file:File):void;
*/
class DataInput extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e) {
    const files = e.target.files;
    if (files && files[0]) this.props.handleFile(files[0]);
  }
  render() {
    return (
      <form className="form-inline">
        <h1 className="title">Reto Neo</h1>
        <div className="form-group">
          <label htmlFor="file">Procesar archivo: </label>
          <input
            type="file"
            className="form-control"
            id="file"
            accept={SheetJSFT}
            onChange={this.handleChange}
          />
        </div>
      </form>
    );
  }
}

/*
  Simple HTML Table
  usage: <OutTable data={data} cols={cols} />
    data:Array<Array<any> >;
    cols:Array<{name:string, key:number|string}>;
*/
class OutTable extends React.Component {

  render() {
    return (
      <div className="table-responsive">
        <table className="table table-striped">
          <thead>
            <tr>
                <th></th>
              {this.props.cols.map(c => (
                <th key={c.key}>{c.name}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {this.props.data.map((r, i) => (
              <tr key={i}>
                <td className="bold">{i+1}</td>
                {this.props.cols.map(c => (
                  <td key={c.key}><input type="text" className="sheet-input" data-row={i} data-col={c.key} value={r[c.key]} onChange={e => this.props.setNewValue(i, c.key, e.target.value)} /></td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

class ContextMenu extends React.Component {
    state = {
        visible: false,
    };
  constructor(props) {
    super(props);
    this._handleContextMenu = this._handleContextMenu.bind(this);
  }

    componentDidMount() {
        document.addEventListener('contextmenu', this._handleContextMenu);
        document.addEventListener('click', this._handleClick);
        document.addEventListener('scroll', this._handleScroll);
    };

    componentWillUnmount() {
      document.removeEventListener('contextmenu', this._handleContextMenu);
      document.removeEventListener('click', this._handleClick);
      document.removeEventListener('scroll', this._handleScroll);
    }

    _handleContextMenu = (event) => {
        this.props.setCoords(event.target.attributes['data-row'].value, event.target.attributes['data-col'].value);
        console.log('aca andamos',)
        event.preventDefault();

        this.setState({ visible: true });

        const clickX = event.clientX;
        const clickY = event.clientY;
        const screenW = window.innerWidth;
        const screenH = window.innerHeight;
        const rootW = this.root.offsetWidth;
        const rootH = this.root.offsetHeight;

        const right = (screenW - clickX) > rootW;
        const left = !right;
        const top = (screenH - clickY) > rootH;
        const bottom = !top;

        if (right) {
            this.root.style.left = `${clickX + 5}px`;
        }

        if (left) {
            this.root.style.left = `${clickX - rootW - 5}px`;
        }

        if (top) {
            this.root.style.top = `${clickY + 5}px`;
        }

        if (bottom) {
            this.root.style.top = `${clickY - rootH - 5}px`;
        }
    };

    _handleClick = (event) => {
        const { visible } = this.state;
        const wasOutside = !(event.target.contains === this.root);

        if (wasOutside && visible) this.setState({ visible: false, });
    };

    _handleScroll = () => {
        const { visible } = this.state;

        if (visible) this.setState({ visible: false, });
    };

    render() {
        const { visible } = this.state;

        return(visible || null) &&
            <div ref={ref => {this.root = ref}} className="contextMenu">
                <div className="contextMenu--option" onClick={() => this.props.cellAdd()}>Agregar celda</div>
                <div className="contextMenu--option" onClick={() => this.props.cellDelete()}>Eliminar celda</div>
                <div className="contextMenu--option" onClick={() => this.props.rowAdd()}>Agregar fila</div>
                <div className="contextMenu--option" onClick={() => this.props.rowDelete()}>Eliminar fila</div>
            </div>
    };
}

/* list of supported file types */
const SheetJSFT = [
  "xlsx",
  "xlsb",
  "xlsm",
  "xls",
  "xml",
  "csv",
  "txt",
  "ods",
  "fods",
  "uos",
  "sylk",
  "dif",
  "dbf",
  "prn",
  "qpw",
  "123",
  "wb*",
  "wq*",
  "html",
  "htm"
]
  .map(function(x) {
    return "." + x;
  })
  .join(",");

/* generate an array of column objects */
const make_cols = refstr => {
  let o = [],
    C = XLSX.utils.decode_range(refstr).e.c + 1;
  for (var i = 0; i < C; ++i) o[i] = { name: XLSX.utils.encode_col(i), key: i };
  return o;
};
